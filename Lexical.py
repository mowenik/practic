def getLex(charArr):
    resultArr = []
    reserved = ['absolute', 'and', 'array', 'as', 'asm', 'begin', 'case', 'class',
                'const', 'constructor', 'destructor', 'dispinterface', 'dispose',
                'div', 'do', 'downto', 'end', 'except', 'exit', 'exports', 'file',
                'finalization', 'finally', 'for', 'function', 'goto', 'implementation',
                'in', 'inherited', 'initialization', 'inline', 'interface', 'is', 'label',
                'library', 'mod', 'new	', 'nil', 'not', 'object', 'of', 'on', 'operator',
                'or', 'out', 'packed', 'procedure', 'program', 'property', 'raise', 'record',
                'reintroduce', 'repeat', 'resourcestring', 'self', 'set', 'shl', 'shr', 'string',
                'threadvar', 'to', 'try', 'type', 'unit', 'until', 'uses', 'var', 'while', 'with', 'xor']
    word = ''
    num = 0

    for item in charArr:
        if item.currentClass == 'LETTER':
            if num != 0:
                resultArr.append('number')
                num = 0
            word += item.symbol
            continue

        if item.currentClass == 'NUMBER':
            if word != '':
                word += item.symbol
            else:
                if num == 0:
                    num = int(item.symbol)
                else:
                    num = int(str(num) + item.symbol)
            continue

        if item.currentClass in ['SPACE', 'LEFT SB', 'RIGHT SB', 'COMMA', 'SEMICOLON', 'DSPACE', 'SYMBOL']:
            notFind = True
            if num != 0:
                resultArr.append('number')
                num = 0

            if word != '':
                if item.currentClass == 'DSPACE':
                    word += item.symbol
                if word.lower() in ['if', 'else', 'then']:
                    resultArr.append(word.lower())
                    notFind = False
                if word.lower() == 'true' or word.lower() == 'false':
                    resultArr.append('bool')
                    notFind = False
                if word.lower() in reserved:
                    resultArr.append('keyword')
                    notFind = False
                if notFind:
                    resultArr.append('id')

            if item.currentClass == 'LEFT SB':
                resultArr.append('left_sb')
            if item.currentClass == 'RIGHT SB':
                resultArr.append('right_sb')
            if item.currentClass == 'COMMA':
                resultArr.append('comma')
            if item.currentClass == 'SEMICOLON':
                resultArr.append('semicolon')
            if item.currentClass == 'SYMBOL':
                resultArr.append('symbol')
            if item.currentClass == 'DSPACE' and word == '':
                resultArr.append('error')

            word = ''
            continue

        resultArr.append('error')
    return resultArr
