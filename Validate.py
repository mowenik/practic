def validate(lexArray):
    validateState = {'start': {'if': 'if'},
                     'if': {'id': 'condition', 'bool': 'condition'},
                     'condition': {'then': 'then'},
                     'then': {'id': 'id_func'},
                     'id_func': {'left_sb': 'left_sb'},
                     'left_sb': {'id': 'id_param', 'number': 'id_param', 'bool': 'id_param',
                                 'right_sb': 'right_sb', 'symbol': 'symbol'},
                     'id_param': {'comma': 'comma', 'right_sb': 'right_sb'},
                     'comma': {'id': 'id_param', 'number': 'id_param', 'symbol': 'symbol'},
                     'symbol': {'number': 'id_param'},
                     'right_sb': {'else': 'else', 'semicolon': 'semicolon'},
                     'else': {'id': 'id_func_2'},
                     'id_func_2': {'left_sb': 'left_sb_2'},
                     'left_sb_2': {'id': 'id_param_2', 'number': 'id_param_2', 'bool': 'id_param_2',
                                   'right_sb': 'right_sb_2', 'symbol': 'symbol_2'},
                     'id_param_2': {'comma': 'comma_2', 'right_sb': 'right_sb_2'},
                     'comma_2': {'id': 'id_param_2', 'number': 'id_param_2', 'symbol': 'symbol_2'},
                     'symbol_2': {'number': 'id_param_2'},
                     'right_sb_2': {'semicolon': 'semicolon'},
                     'semicolon': {'err': 'err'}}

    currentState = 'start'
    result = 'REJECT'

    print(lexArray)

    for currentWord in lexArray:
        try:
            currentState = validateState[currentState][currentWord]
            if currentState == 'semicolon':
                result = 'ACCEPT'
        except:
            result = 'REJECT'
            break

    print(currentState)
    return result
