def getData():
    return open('input.txt', 'r').read()


def saveData(str):
    return open('output.txt', 'w').write(str)
