from SymbolModel import SymbolModel


def checkString(str):
    result = []
    for char in str:
        if 'a' <= char <= 'z' or 'A' <= char <= 'Z':
            result.append(SymbolModel(char, 'LETTER'))
            continue
        if '0' <= char <= '9':
            result.append(SymbolModel(char, 'NUMBER'))
            continue
        if char == ' ':
            result.append(SymbolModel(char, 'SPACE'))
            continue
        if char == '=':
            result.append(SymbolModel(char, 'EQ'))
            continue
        if char == '+' or char == '-':
            result.append(SymbolModel(char, 'SYMBOL'))
            continue
        if char == '(':
            result.append(SymbolModel(char, 'LEFT SB'))
            continue
        if char == ')':
            result.append(SymbolModel(char, 'RIGHT SB'))
            continue
        if char == ';':
            result.append(SymbolModel(char, 'SEMICOLON'))
            continue
        if char == ',':
            result.append(SymbolModel(char, 'COMMA'))
            continue
        if char == '_':
            result.append(SymbolModel(char, 'DSPACE'))
            continue

        result.append(SymbolModel(char, 'ERROR'))

    result.append(SymbolModel(' ', 'SPACE'))

    return result
