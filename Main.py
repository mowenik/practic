import Transliteration
import FileHelper
import Lexical
import Validate

transliteration = Transliteration.checkString(FileHelper.getData())
lexArray = Lexical.getLex(transliteration)
result = Validate.validate(lexArray)

FileHelper.saveData(result)
